import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {ErrorService, RequestNotFoundError} from '@universis/common';
import {Subscription} from 'rxjs';
import {RequestsService} from '../../../services/requests.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-preview-exam-period-participate-action',
  templateUrl: './preview.component.html'
})
export class ExamPeriodParticipateActionPreviewComponent implements OnInit {

  public data: any;
  public loading = true;
  constructor(private _context: AngularDataContext) {
  }
  @Input() id: any ;

  async  ngOnInit() {
    const result = await this._context.model('ExamPeriodParticipateActions').where('id').equal(this.id)
      .expand('courseExamActions($expand=courseExam($expand=course,year,examPeriod))')
      .getItem();
    if (result == null) {
      throw new RequestNotFoundError();
    }
    // set reject for ui
    result.reject = !result.agree;
    this.data = result;
  }

}


@Component({
  selector: 'app-exam-period-participate-action-container',
  template: `
            <div *ngIf="id">
                <app-preview-exam-period-participate-action [id]="id"></app-preview-exam-period-participate-action>
            </div>
    `
})
// tslint:disable-next-line:component-class-suffix
export class ExamPeriodParticipateActionContainer implements OnInit, OnDestroy {
  public id;
  private dataSubscription: Subscription;
  constructor(private _requestService: RequestsService,
              private _activatedRoute: ActivatedRoute,
              private _errorService: ErrorService) {
    //
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.dataSubscription = this._activatedRoute.params.subscribe((params) => {
      this.id = params.id;
    }, (err) => {
      return this._errorService.navigateToError(err);
    });
  }

}
