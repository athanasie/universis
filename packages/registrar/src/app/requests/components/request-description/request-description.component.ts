import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-request-description',
  templateUrl: './request-description.component.html'
})
export class RequestDescriptionComponent implements OnInit {

  @Input('request') request;

  constructor() { }

  ngOnInit() {
  }

}
