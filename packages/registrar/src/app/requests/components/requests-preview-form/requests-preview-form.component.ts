import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-requests-preview-form',
  templateUrl: './requests-preview-form.component.html'
})
export class RequestsPreviewFormComponent implements OnInit {

  @Input() model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

  }

}
