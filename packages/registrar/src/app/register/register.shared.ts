import {NgModule, CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { AdvancedFormsModule } from '@universis/forms';
import { RouterModule } from '@angular/router';
import { environment } from '../../environments/environment';
import { MostModule } from '@themost/angular';
import { EditComponent } from './components/edit/edit.component';
import { SidePreviewComponent } from './components/side-preview/side-preview.component';
import { AttachmentDownloadComponent } from './components/attachment-download/attachment-download.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { RequestsSharedModule } from '../requests/requests.shared';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { LatestMessagesComponent } from './components/latest-messages/latest-messages.component';
import { SendMessageActionComponent } from './components/send-message-action/send-message-action.component';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    MostModule,
    RouterModule,
    TranslateModule,
    TablesModule,
    FormsModule,
    AdvancedFormsModule,
    NgxExtendedPdfViewerModule,
    NgxDropzoneModule,
    RequestsSharedModule
  ],
  declarations: [
    EditComponent,
    ModalEditComponent,
    SidePreviewComponent,
    AttachmentDownloadComponent,
    ComposeMessageComponent,
    LatestMessagesComponent,
    SendMessageActionComponent
  ],
  exports: [
    EditComponent,
    ModalEditComponent,
    SidePreviewComponent,
    AttachmentDownloadComponent,
    ComposeMessageComponent,
    LatestMessagesComponent,
    SendMessageActionComponent
  ],
  entryComponents: [
    ModalEditComponent,
    SendMessageActionComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegisterSharedModule implements OnInit  {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading translations');
      console.error(err);
    });
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RegisterSharedModule,
      providers: [
      ],
    };
  }
  async ngOnInit() {
    environment.languages.forEach(language => {
      import(`./i18n/register.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
