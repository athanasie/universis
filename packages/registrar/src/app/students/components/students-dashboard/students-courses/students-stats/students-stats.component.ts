import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { transform } from 'lodash';

@Component({
  selector: 'app-students-stats',
  templateUrl: './students-stats.component.html',
  styleUrls: ['./students-stats.component.scss']
})
export class StudentsStatsComponent implements OnInit, OnDestroy  {

  public courseTypes: any;
  public summary: any;
  @Input() studentId: number;
  @Input() showMore: boolean;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translate: TranslateService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      const courseTypeGroups = await this._context.model('Students/' + this.studentId + '/courses')
        .select('count(id) as total', 'sum(ects) as ects', 'courseType', 'calculateUnits', 'calculateGrade')
        .expand('courseType($select=id,abbreviation,locale)')
        .groupBy('courseType,calculateUnits,calculateGrade')
        .where('isPassed').equal(1).and('courseStructureType').notEqual(8)
        .getItems();

      const tmp = { ects: 0, total: 0 };
      const calcTmp = { calculatedEcts: 0, calculatedTotal: 0 };
      this.courseTypes = {};
      this.summary = { both: { ...tmp }, none: { ...tmp }, onlyUnits: { ...tmp }, onlyGrade: { ...tmp }, total: 0, ects: 0 };

      courseTypeGroups.forEach(group => {
        const typeAbbreviation = group.courseType.locale ? group.courseType.locale.abbreviation : group.courseType.abbreviation;
        if (!this.courseTypes.hasOwnProperty(typeAbbreviation)) {
          this.courseTypes[typeAbbreviation] = { ...tmp, ...calcTmp };
        }

        const courseType = this.courseTypes[typeAbbreviation];
        const calculated = group.calculateUnits
          ? group.calculateGrade ? 'both' : 'onlyUnits'
          : group.calculateGrade ? 'onlyGrade' : 'none';

        if (calculated === 'onlyUnits' || calculated === 'both') {
          courseType.calculatedEcts += group.ects;
          courseType.calculatedTotal += group.total;
        }

        courseType['ects'] += group.ects;
        courseType['total'] += group.total;
        this.summary[calculated]['ects'] += group.ects;
        this.summary[calculated]['total'] += group.total;
        this.summary.total += group.total;
        this.summary.ects += group.ects;
      });

      this.summary.show = this.summary.both.total !== this.summary.total;
      if (this.summary.show) { this.setSummaryMessage(); }
    });
  }

  setSummaryMessage() {
    let message = this._translate.instant('Students.PassedSummary.Total', {
      total: this.summary.total,
      both: this.summary.both.total
    });

    const messagesToTranslate = [
      {
        title: 'None',
        display: this.summary.none.total > 0,
        obj: { none: this.summary.none.total }
      },
      {
        title: 'Grade',
        display: this.summary.onlyGrade.total > 0,
        obj: { onlyGrade: this.summary.onlyGrade.total }
      },
      {
        title: 'Units',
        display: this.summary.onlyUnits.total > 0,
        obj: { onlyUnits: this.summary.onlyUnits.total }
      }
    ];

    for (const msg of messagesToTranslate) {
      if (msg.display) {
        message += this._translate.instant(`Students.PassedSummary.${[msg.title]}`, msg.obj);
      }
    }

    const lastComma = message.lastIndexOf(',') + 1;
    const linkingWord = this._translate.instant('Students.PassedSummary.LinkingWord');
    this.summary.message = message.slice(0, lastComma) + ' ' + linkingWord + message.slice(lastComma) + '.';
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
