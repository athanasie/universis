import { Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { GraduationService } from '@universis/graduation';

@Component({
  selector: 'app-students-degree-calculation',
  templateUrl: './students-degree-calculation.component.html'
})
export class StudentsDegreeCalculationComponent implements OnChanges {

  @Input() public student: any;
  public degree: any;
  public lastError: any = null;
  @Input() public requirementsAreMet: boolean = null;


  constructor(private _graduationService: GraduationService,
              private _context: AngularDataContext) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.lastError = null;
    if (changes.student) {
      if (changes.student.currentValue == null) {
        this.student = null;
        this.degree = {};
        return;
      }
      this.student = changes.student.currentValue;
      this._context.model(`Students/${this.student}/CalculateGraduationDegree`).save('').then(degree => {
        this.degree = degree;
      }).catch(err => {
        this.degree = {};
        console.error(err);
        this.lastError = err;
      });
    }
    if (changes.requirementsAreMet) { this.requirementsAreMet = changes.requirementsAreMet.currentValue; }
  }

}
