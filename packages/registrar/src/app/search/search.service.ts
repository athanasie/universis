import { Injectable, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, TemplatePipe } from "@universis/common";
import * as SpotlightConfig from "./search.config.list.json";

@Injectable({ providedIn: 'root' })
export class SearchService {

  constructor(private _context: AngularDataContext,
    private _template: TemplatePipe,
    private _errorService: ErrorService) { }

  async searchEntity<T>(term: string, entity: string, callback: (element: T) => any): Promise<T[]> {
    let searchTerm = term.trim();
    let searchConfig: any;

    if (!searchTerm) {
      // if no search term, return empty student array
      return [];
    }
    if (searchTerm.length > 2) {
      try {
        // Get config 
        const config = JSON.parse(JSON.stringify(SpotlightConfig));
        config.forEach(element => {
          if (element.entitySet == entity) {
            searchConfig = element;
          }
        });
        const options = searchConfig.options;
        //
        const entityQuery = this._context.model(entity)
          .asQueryable()
          .expand('department')
          .select(options["$select"])
          .take(options["$top"])
          .orderBy(options["$orderby"]);

        entityQuery.setParam('$filter',
          this._template.transform(options["$filter"], {
            text: searchTerm
          }));
        let resultsP = entityQuery.getItems();

        return resultsP.then(results => results.map((result: T) => {
          let values = callback(result);
          result['label'] = this._template.transform(searchConfig.label, values);
          result['type'] = entity;
          return result;
        }));

      } catch (err) {
        console.error(err);
        this._errorService.showError(err, {
          continueLink: '.'
        });
        return [];
      }
    } else {
      return [];
    }
  }


}
