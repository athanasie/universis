import { CommonModule } from "@angular/common";
import { ModuleWithProviders, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "@universis/common";
import { ModalModule } from "ngx-bootstrap/modal";
import { NgArrayPipesModule, NgPipesModule } from "ngx-pipes";
import { UserQueryHistoryComponent } from "./components/user-query-history.component";

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      NgPipesModule,
      NgArrayPipesModule,
      SharedModule,
      TranslateModule,
      ModalModule
  ],
  declarations: [
    UserQueryHistoryComponent
  ],
  exports: [
    UserQueryHistoryComponent
  ],
  entryComponents: [
    UserQueryHistoryComponent
  ]
})
export class QueryBuilderHelperModule {

  static forRoot(): ModuleWithProviders<QueryBuilderHelperModule> {
    return {
        ngModule: QueryBuilderHelperModule
    };
    }

  constructor() {
    //
  }


}
