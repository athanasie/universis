import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from "@angular/core";
import { UserQuery } from "../model";
import * as moment from 'moment';
import { ApplicationDatabase } from "../../registrar-shared/services/app-db.service";
import { Observable, ReplaySubject } from "rxjs";
import { AppEventService } from "@universis/common";

declare interface UserQueryWithDate extends UserQuery {
  dateOnlyAccessed: Date;
}

@Component({
  selector: 'user-query-history',
  templateUrl: './user-query-history.component.html',
  styles: [
    `
      .modal-body .history-group:not(:first-child) {
        margin-top: 16px;
      }
    `
  ]
})
export class UserQueryHistoryComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() entityType: string;

  @Input() viewItems: UserQueryWithDate[] = [];
  public loading = true;

  @Output() closing = new EventEmitter();

  @Output() navigate = new EventEmitter<{
    target: UserQueryHistoryComponent,
    item: UserQuery
  }>(null);
  public itemSource = new ReplaySubject<UserQuery[]>(1);
  public items$: Observable<UserQuery[]> = this.itemSource.asObservable();
  
  constructor(private applicationDatabase: ApplicationDatabase, private appEvent: AppEventService) {
    
  }

  ngAfterViewInit(): void {
    this.applicationDatabase.db.table('UserQuery').where('entityType').equals('Student').toArray().then((results) => {
      const items = results.map((item) => {
        return Object.assign(item, {
          dateOnlyAccessed: moment(item.dateAccessed).startOf('date').toDate()
        });
      }).sort((a, b) => {
        if (a.dateAccessed > b.dateAccessed) {
          return -1;
        }
        if (a.dateAccessed < b.dateAccessed) {
          return -1;
        }
        return 0;
      });
      this.itemSource.next(items);
    });
  }
  
  ngOnInit() {
    //
  }

  ngOnDestroy(): void {
    //
  }

  remove(item: UserQuery) {
    this.applicationDatabase.db.table('UserQuery').delete(item.id).then(() => {
      this.ngAfterViewInit();
      this.appEvent.remove.next({
        model: 'UserQuery',
        target: item
      })
    });
  }

  onNavigate(item: UserQuery) {
    const target = this;
    this.navigate.emit({
      target,
      item
    })
  }
}

