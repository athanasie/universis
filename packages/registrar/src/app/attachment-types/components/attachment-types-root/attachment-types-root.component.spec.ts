import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentTypesRootComponent } from './attachment-types-root.component';

describe('AttachmentTypesRootComponent', () => {
  let component: AttachmentTypesRootComponent;
  let fixture: ComponentFixture<AttachmentTypesRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentTypesRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentTypesRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
