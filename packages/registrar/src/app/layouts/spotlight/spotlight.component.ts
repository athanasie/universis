import { Component, HostListener, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalContentComponent } from '../../search/component/modal-content/modal-content.component';

@Component({
  selector: 'app-spotlight',
  templateUrl: './spotlight.component.html'
})
export class SpotlightComponent {
  bsModalRef?: BsModalRef;
  constructor(private modalService: BsModalService) { }

  @HostListener('window:keydown.control.shift.f', ['$event'])
  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(ModalContentComponent, { class: 'modal-lg' });
  }
}
