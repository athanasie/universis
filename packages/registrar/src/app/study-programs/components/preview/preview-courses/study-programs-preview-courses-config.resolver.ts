import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class StudyProgramsPreviewCoursesConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./study-programs-preview-courses.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./study-programs-preview-courses.config.list.json`);
        });
    }
}

export class StudyProgramsPreviewCoursesSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./study-programs-preview-courses.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./study-programs-preview-courses.search.list.json`);
            });
    }
}

export class StudyProgramsDefaultPreviewCoursesConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./study-programs-preview-courses.config.list.json`);
    }
}
