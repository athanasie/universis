import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewGeneralComponent } from './exams-preview-general.component';

describe('ExamsPreviewGeneralComponent', () => {
  let component: ExamsPreviewGeneralComponent;
  let fixture: ComponentFixture<ExamsPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
