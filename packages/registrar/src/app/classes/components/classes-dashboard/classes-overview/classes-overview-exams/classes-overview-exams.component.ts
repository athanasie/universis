import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-overview-exams',
  templateUrl: './classes-overview-exams.component.html',
  styleUrls: ['./classes-overview-exams.component.scss']
})
export class ClassesOverviewExamsComponent implements OnInit, OnDestroy {

  public classExams: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.classExams = await this._context.model('CourseExamClasses')
        .where('courseClass').equal(params.id)
        .expand('courseExam($expand=examPeriod,status,completedByUser,year,course($expand=department))')
        .orderByDescending('courseExam/year')
        .thenByDescending('courseExam/examPeriod')
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
