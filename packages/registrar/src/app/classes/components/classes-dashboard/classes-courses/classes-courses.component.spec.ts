import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesCoursesComponent } from './classes-courses.component';

describe('ClassesCoursesComponent', () => {
  let component: ClassesCoursesComponent;
  let fixture: ComponentFixture<ClassesCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
