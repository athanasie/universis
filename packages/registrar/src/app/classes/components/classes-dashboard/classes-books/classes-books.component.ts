import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import * as CLASSES_BOOKS_LIST_CONFIG from './classes-books.config.list.json';
import { DiagnosticsService, ErrorService } from '@universis/common';
import { Subscription } from 'rxjs';
import { ActivatedTableService } from '@universis/ngx-tables';
// tslint:disable-next-line:max-line-length

@Component({
  selector: 'app-classes-books',
  templateUrl: './classes-books.component.html',
  styleUrls: ['./classes-books.component.scss']
})
export class ClassesBooksComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>CLASSES_BOOKS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('books') books: AdvancedTableComponent;
  courseClassID: any;
  private fragmentSubscription: Subscription;
  @Input() tableConfiguration: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _diagnosticsService: DiagnosticsService) { }

  async ngOnInit() {
    this._diagnosticsService.hasService('EudoxusService').then((result) => {
      if (result === false) {
        return this._errorService.showError({ continueLink: '.' });
      }
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.courseClassID = params.id;
        this._activatedTable.activeTable = this.books;

        this.books.query = this._context.model('Books')
          .where('courseClass')
          .equal(this.courseClassID)
          .prepare();

        this.books.config = AdvancedTableConfiguration.cast(CLASSES_BOOKS_LIST_CONFIG);
        this.books.fetch();

        this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
          if (fragment && fragment === 'reload') {
            this.books.fetch(true);
          }
        });

        this.dataSubscription = this._activatedRoute.data.subscribe(data => {
          if (data.tableConfiguration) {
            this.books.config = data.tableConfiguration;
            this.books.ngOnInit();
          }
        });
      });
    }).catch((err) => {
      console.error(err);
        this._errorService.showError(err, {
          continueLink: '.'
        });
    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
