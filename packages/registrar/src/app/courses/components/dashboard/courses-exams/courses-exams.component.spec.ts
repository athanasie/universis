import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesExamsComponent } from './courses-exams.component';

describe('CoursesExamsComponent', () => {
  let component: CoursesExamsComponent;
  let fixture: ComponentFixture<CoursesExamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesExamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
