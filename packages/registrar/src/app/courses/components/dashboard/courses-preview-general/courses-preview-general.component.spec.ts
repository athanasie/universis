import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesPreviewGeneralComponent } from './courses-preview-general.component';

describe('CoursesPreviewGeneralComponent', () => {
  let component: CoursesPreviewGeneralComponent;
  let fixture: ComponentFixture<CoursesPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
