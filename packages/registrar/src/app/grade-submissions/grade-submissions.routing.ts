import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GradeSubmissionsHomeComponent } from './components/grade-submissions-home/grade-submissions-home.component';
import { GradeSubmissionsTableComponent } from './components/grade-submissions-table/grade-submissions-table.component';
import { GradeSubmissionsTableConfigurationResolver, GradeSubmissionsTableSearchResolver } from './components/grade-submissions-table/grade-submissions-table-config.resolver';
import { CurrentAcademicYearResolver } from '../registrar-shared/services/activeDepartmentService.service';

const routes: Routes = [
  {
    path: '',
    component: GradeSubmissionsHomeComponent,
    data: {
      title: 'GradeSubmissions'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/pending'
      },
      {
        path: 'list/:list',
        component: GradeSubmissionsTableComponent,
        data: {
          title: 'grade-submissions List'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          tableConfiguration: GradeSubmissionsTableConfigurationResolver,
          searchConfiguration: GradeSubmissionsTableSearchResolver
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class GradeSubmissionsRoutingModule {
}
