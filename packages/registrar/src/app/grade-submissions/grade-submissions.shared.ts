import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { StudentsSharedModule } from '../students/students.shared';
import * as DEFAULT_EXAMS_LIST from './components/grade-submissions-table/grade-submissions-table.config.json';
import { CoursesSharedModule } from '../courses/courses.shared';
import { GradeSubmissionsDefaultTableConfigurationResolver, GradeSubmissionsTableSearchResolver, GradeSubmissionsTableConfigurationResolver } from './components/grade-submissions-table/grade-submissions-table-config.resolver';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    StudentsSharedModule,
    CoursesSharedModule
  ],
  declarations: [
  ],
  exports: [
  ],
  providers: [
    GradeSubmissionsDefaultTableConfigurationResolver,
    GradeSubmissionsTableSearchResolver,
    GradeSubmissionsTableConfigurationResolver,
  ]
})
export class GradeSubmissionsSharedModule implements OnInit {
  //public static readonly TestTypesList = TEST_TYPES_LIST;
  public static readonly DefaultExamList = DEFAULT_EXAMS_LIST;
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch((err) => {
      console.error('An error occurred while loading exams shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach((language) => {
      import(`./i18n/grade-submissions.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
