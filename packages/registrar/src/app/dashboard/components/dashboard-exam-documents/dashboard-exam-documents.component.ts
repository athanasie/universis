import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActiveDepartmentService} from "../../../registrar-shared/services/activeDepartmentService.service";

@Component({
  selector: 'app-dashboard-exam-documents',
  templateUrl: './dashboard-exam-documents.component.html',
  styleUrls: ['./dashboard-exam-documents.component.scss']
})
export class DashboardExamDocumentsComponent implements OnInit {

  public uploadGrades: any;

  constructor(private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService ) { }

  async ngOnInit() {

    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();

    this.uploadGrades = await this._context.model('ExamDocumentUploadActions')
      .where('actionStatus/alternateName ').equal('ActiveActionStatus')
      .and('additionalResult').notEqual(null)
      .and('object/course/department').equal(activeDepartment.id)
      .expand('owner,object($expand=year,examPeriod,course)')
      // .expand('additionalResult,createdBy')
      .orderByDescending('dateModified')
      .take(3)
      .getList();
  }

}
